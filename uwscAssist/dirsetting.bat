@echo off

set CID=%~1
set CNAME=%~2
set WORKDIR=%~3
set FILEPATH1=%~4
set FILEPATH2=%~5
set FILEPATH3=%~6


pushd "%WORKDIR%"

if exist データ\%CID% (goto MSG)

mkdir データ\%CID%
mkdir データ\%CID%\tools

tools\csc\csc.exe "%WORKDIR%\データ\%CID%" "設定中\%CID%-%CNAME%.lnk"

copy tools\*.* データ\%CID%\tools\
move データ\%CID%\tools\企画テスト作成.xlsm データ\%CID%\%CID%企画テスト作成.xlsm
move データ\%CID%\tools\過去の企画テスト.lnk データ\%CID%\過去の企画テスト.lnk
copy "%FILEPATH1%" データ\%CID%\
copy "%FILEPATH2%" データ\%CID%\
copy "%FILEPATH3%" データ\%CID%\
echo %CNAME% >> データ\%CID%\tools\cname.txt

explorer "%WORKDIR%\データ\%CID%"

goto END

:MSG

echo "そのキャンペーンIDは既に存在します。"
timeout 3

popd
:END